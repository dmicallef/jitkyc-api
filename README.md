JitKYC API is written in using Django REST Framework.


Summary
```````
1. Create Virtual environment and install requirements
2. Run ```python manage.py migrate``` to migrate models to db
3. Run ```python manage.py runserver``` . This should run the
    application on `localhost:8000`


SuperUser
admin@jitkyc.com
Adm1nU$3r